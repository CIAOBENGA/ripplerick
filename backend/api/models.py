from django.db import models
from rest_framework import serializers
from jsonfield import JSONField

class Message(models.Model):
    subject = models.CharField(max_length=200)
    body = models.TextField()


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ('url', 'subject', 'body', 'pk')

class Nft(models.Model):
    nft_id = models.AutoField(primary_key=True)
    uuid = models.CharField(max_length=200, default='123e4567-e89b-12d3-a456-426614174000')
    title=models.CharField(max_length=200, default='Untitled')
    account = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    royalties = models.CharField(max_length=200, blank=True, null=True)
    ticket = models.BooleanField(default=False)
    nsfw = models.BooleanField(default=False)
    codec = models.CharField(max_length=200, default='image')
    ipfs=  models.TextField(blank=True, null=True)
    tx =  models.TextField(blank=True, null=True)
    nfts =  models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
        


class NftSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Nft
        fields = ('account', 'uuid', 'title', 'description', 'royalties', 'ticket', 'nsfw', 'codec', 'ipfs', 'tx', 'nfts')
