from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework import viewsets
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.forms.models import model_to_dict

from .models import Message, MessageSerializer, Nft, NftSerializer
import http.client
import json
import xumm

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class NftViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Nft.objects.order_by('-created_at')
    serializer_class = NftSerializer


@csrf_exempt
def RetrieveNFT(request):
    nft = Nft.objects.get(uuid=request.POST.get('uuid')) 
    if nft:
        serializer = NftSerializer(nft)
        return HttpResponse(json.dumps(serializer.data))
    else:
        return HttpResponse(False)

@csrf_exempt
def InitX(request):
    sdk = xumm.XummSdk('f9d0aed0-4db5-42f1-9459-3c1452312d2b', 'ee45f4aa-61a3-4ab6-91a3-248a0d86d608')
    payload = {
        "txjson": {
          "TransactionType": "SignIn"
        }
    }
    pong = sdk.ping()

    created = sdk.payload.create(payload)

    return HttpResponse(json.dumps({'qrurl': created.next.always, 'websocket': created.refs.websocket_status}))

@csrf_exempt
def RetrieveAcc(request):
    sdk = xumm.XummSdk('f9d0aed0-4db5-42f1-9459-3c1452312d2b', 'ee45f4aa-61a3-4ab6-91a3-248a0d86d608')

    created = sdk.payload.get(request.POST.get('uuid'))

    return HttpResponse(json.dumps({'acc': created.response.account, 'user_token': created.application.issued_user_token }))

@csrf_exempt
def signNFTMint(request):
    sdk = xumm.XummSdk('f9d0aed0-4db5-42f1-9459-3c1452312d2b', 'ee45f4aa-61a3-4ab6-91a3-248a0d86d608')
    payload = {
        "txjson": 
        {
          "TransactionType": "NFTokenMint",
          "Account": "rppePk2CDpY2zCujnUFTFKkfb8PhMYHgok",
          "TokenTaxon": 0,
          "URI": "697066733a2f2f62616679626569676479727a74357366703775646d37687537367568377932366e6634646675796c71616266336f636c67747179353566627a6469",
        }
    }

    pong = sdk.ping()

    created = sdk.payload.create(payload)

    return HttpResponse(json.dumps({'qrurl': created.next.always, 'websocket': created.refs.websocket_status}))
