"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.conf import settings     
from django.conf.urls.static import static 
from django.views.generic import TemplateView
from django.urls import path, re_path

from .api.views import *

router = routers.DefaultRouter()
router.register('messages', MessageViewSet)
router.register('nfty', NftViewSet)

urlpatterns = [

    # http://localhost:8000/
    # path('', index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls),

    path('api/retrieve/nft', RetrieveNFT, name='retrieve_nft'),

    path('api/retrieve/acc', RetrieveAcc, name='retrieve_acc'),

    path('api/sign/mint', signNFTMint, name='signNFTMint'),

    path('api/init/tx', InitX, name='init_tx'),


    re_path(r'^.*$', TemplateView.as_view(template_name="index.html")),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:     
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)   
    # urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
    urlpatterns += static('/', document_root='dist')