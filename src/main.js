import { createApp } from 'vue'
// import the root component App from a single-file component.
import App from './App.vue'

import VueFeather from 'vue-feather';

import AudioVisual from 'vue-audio-visual'

import SolanaWallets from 'solana-wallets-vue';

// You can either import the default styles or create your own.
import 'solana-wallets-vue/styles.css';

import { WalletAdapterNetwork } from "@solana/wallet-adapter-base"

import {
  PhantomWalletAdapter,
  SlopeWalletAdapter,
  SolflareWalletAdapter,
} from '@solana/wallet-adapter-wallets';

const walletOptions = {
  wallets: [
    new PhantomWalletAdapter(),
    new SlopeWalletAdapter(),
    new SolflareWalletAdapter({ network: WalletAdapterNetwork.Devnet }),
  ],
  autoConnect: true,
}

import 'vuetify/styles' // Global CSS has to be imported
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
  components,
  directives,
})

// Routing.
import { createRouter, createWebHashHistory } from 'vue-router'

import router from './routes' // <---

import store from './store.js'

import './index.css'

import Rubric from './components/Rubric.vue'

import Navbar from './components/Navbar.vue'

import Sidebar from './components/Sidebar.vue'

import Bottombar from './components/Bottombar.vue'

console.log(store)

const app = createApp(App)

app.use(SolanaWallets, walletOptions)
app.use(vuetify)
app.use(router)
app.use(store)
app.use(AudioVisual)

app.component('Rubric', Rubric)
app.component('Navbar', Navbar)
app.component('Sidebar', Sidebar)
app.component('Bottombar', Bottombar)
app.component(VueFeather.name, VueFeather);

app.mount('#app')

