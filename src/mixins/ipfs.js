import axios from 'axios'
import Cookies from 'js-cookie'
import { v4 as uuidv4 } from 'uuid'

const ipfs = {
	methods: {
		uploadToIPFS(){
			const Upload = (file, fileName, callbackProgress, callback) => {
			  const config = {
			    onUploadProgress: function (progressEvent) {
			      var percentCompleted = Math.round(
			        (progressEvent.loaded * 100) / progressEvent.total
			      )
			      callbackProgress(percentCompleted)
			    },
			    maxBodyLength: 'Infinity',
			   
			  }

			  //You'll need to make sure that the metadata is in the form of a JSON object that's been convered to a string
			  //metadata is optional
			  const metadata = JSON.stringify({
			      name: fileName,
			      keyvalues: {
			          exampleKey: uuid
			      }
			  })

			 
			  //pinataOptions are optional
			  const pinataOptions = JSON.stringify({
			      cidVersion: 0,
			      customPinPolicy: {
			          regions: [
			              {
			                  id: 'FRA1',
			                  desiredReplicationCount: 1
			              },
			              {
			                  id: 'NYC1',
			                  desiredReplicationCount: 2
			              }
			          ]
			      }
			  })

			  let data = new FormData()

			  const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`

			  data.append('file', file)
				data.append('pinataOptions', pinataOptions)
				data.append('pinataMetadata', metadata)

				axios.post(url, data, config).then( (res)=> {
	 			 		callback(res)
	 			 }).catch( (error) => {
	 			 		console.log(error)
	 			})
			}
			// const url = `https://api.pinata.cloud/data/testAuthentication`

			var blob = this.$store.state.file
			var uuid = uuidv4()


			var str = blob.type.split('/')
			var fileName = uuid + '.' + str[1]


			var file = new File([blob], fileName, {type: blob.type})

			this.$store.commit('SET_UPLOADING', true)

			Upload(file, fileName, (percent) => {
				// this.uploadProgress = percent
				this.$store.commit('SET_PROGRESS', percent) 
			}, (result) => {
				this.$store.state.ipfsData = result.data
			})			
		}		
	}
}

export default ipfs