import api from './api'
const mintXRP = {
	data: () => {
		return {
			somedata: null,
		}
	},
	computed: {
	  ipfsData () {
	    return this.$store.state.ipfsData
	    // Or return basket.getters.fruitsCount
	    // (depends on your design decisions).
	  },
	  acc(){
	  	return this.$store.state.acc
	  }
	},

	watch: {
		ipfsData () {
		    this.mintXRPToken()
		}
	},

	methods: {
		//generate wallet and secret credentials if non existant

		async mintXRPToken() {
			const xrpl = require("xrpl")	
			const store = this.$store.state
			const seed = (store.seed == null)  ? 'sneDdgPUTdzAqq8zqLov9hq7Chugj' : store.seed

		    const wallet = xrpl.Wallet.fromSeed(seed)
		    const client = new
		         xrpl.Client("wss://xls20-sandbox.rippletest.net:51233")
		    await client.connect()
		    console.log("Connected to devnet")

		    const transactionBlob = {
		        TransactionType: "NFTokenMint",
		        Account: wallet.classicAddress,
		        URI: xrpl.convertStringToHex(`ipfs://${this.ipfsData.IpfsHash}`),
		        Flags: parseInt(8),
		        TokenTaxon: 0
		    }


		    // var f = new FormData()

		    // f.append('acc', this.acc)
		    // f.append('URI', 'ipfs://QmabcWxHkPcJyhxwSsNDUt4M9eabmcRjG6wPNqucRH7mvh')

		    // this.$store.dispatch('signNFTMint', f)

		    const tx = await client.submitAndWait(transactionBlob,{wallet})

		    const nfts = await client.request({
		        method: "account_nfts",
		        account: wallet.classicAddress  
		    })
		    console.log(nfts)

		    console.log("Transaction result:", tx.result.meta.TransactionResult)
		    console.log("Balance changes:",
		      JSON.stringify(xrpl.getBalanceChanges(tx.result.meta), null, 2))

		    client.disconnect()

		    this.$store.dispatch('postNfty', {
		    	account: wallet.classicAddress,
		    	uuid: tx.result.hash,
		    	description: store.description,
		    	royalties: store.royalties,
		    	ticket: store.ticket,
		    	nsfw: store.nsfw,
		    	codec: store.codec,
		    	ipfs: JSON.stringify(this.ipfsData),
		    	tx: JSON.stringify(tx),
		    	nfts: JSON.stringify(nfts)
		    })

		    this.$store.commit('SET_UPLOADING', false)
		},

	}
}

export default mintXRP