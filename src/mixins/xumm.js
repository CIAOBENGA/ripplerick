import {verifySignature} from 'verify-xrpl-signature'

const signTx = {
	methods: {
		async getAcc(){
			this.$store.dispatch('initTx', {})
		},
		rAcc(){
			var f = new FormData()
			f.append('uuid', this.wsd.payload_uuidv4)
			this.$store.dispatch('retrieveAcc', f)
		}
	},
	computed: {
		initTxWs(){
			return this.$store.state.initTxWs
		},
		wsd(){
			return this.$store.state.wsd
		}
	},
	watch: {
		initTxWs(x){
			this.connection = new WebSocket(x)

			this.connection.onmessage = (event) => {
			  if (JSON.parse(event.data).hasOwnProperty('txid')) {
			  	this.$store.commit('SET_WSD', JSON.parse(event.data) )
			  }
			}
		},
		wsd(x){
			this.rAcc()
		}
	}
}

export default signTx
