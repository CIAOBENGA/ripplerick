import { createWebHistory, createRouter } from "vue-router";
import Home from "@/pages/Home.vue";
import Upload from "@/pages/Upload.vue";
import Nfty from '@/pages/Nfty'
import QR from '@/pages/QR'

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/upload",
    name: "Upload",
    component: Upload,
  },
  {
    path: "/nfty/:account",
    name: "Nfty",
    component: Nfty,
  },
  {
    path: "/nfty/:account",
    name: "Nfty",
    component: Nfty,
  },
  {
    path: "/qr",
    name: "QR",
    component: QR,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;