// store.js
import { createStore } from 'vuex'
// import axios from 'axios'
// import Cookies from 'js-cookie' 
// import router from '@/routes'
import api from '@/mixins/api'
import Cookies from 'js-cookie'

export default new createStore({
  state () {
    return { 
      count: 0,
      file: null,
      uploading:false,
      progress:0,
      ipfsData:null,
      account: null,
      seed: null,
      description: null,
      royalties: null,
      ticket: false,
      nsfw: false,
      tx: null,
      nfts: null,
      codec: 'image',
      remember: true,
      feed: null,
      nft: null,
      initTxQurl: null,
      initTxWs: null,
      wsd: null,
      acc: null,
      utoken: null
    }
  },
  actions: {
    postNfty({commit}, payload) {
      return api.post(`nfty/`, payload).then(response => response.data)
    },
    fetchNfts({commit}, payload){
      return api.get(`nfty/`).then(response => commit('SET_FEED', response.data))
    },
    retrieveNFT({commit}, payload){
      return api.post(`retrieve/nft`, payload).then(response => commit('SET_NFT', response.data))
    },
    initTx({commit}, payload){
      return api.post(`init/tx`, payload).then(response=> {
        commit(`SET_INITX`, response.data.qrurl)
        commit(`SET_INITX_WS`, response.data.websocket)
      })
    },
    retrieveAcc({commit}, payload){
      return api.post(`retrieve/acc`, payload).then(response => {
        commit('SET_ACC', response.data.acc)
        commit('SET_UTOKEN', response.data.user_token)
        if(this.state.remember){
          Cookies.set('acc', response.data.acc)
          Cookies.set('utoken', response.data.user_token)
        }
      })
    },
    signNFTMint({commit}, payload){
      return api.post(`sign/mint`, payload).then(response=> {
        commit(`SET_INITX`, response.data.qrurl)
        commit(`SET_INITX_WS`, response.data.websocket)
      })
    }
  },
  mutations: {
    SET_FILE(state, file){
      state.file = file
    },
    SET_SEED(state, seed){
      state.seed = seed
    },
    SET_DESCRIPTION(state, description){
      state.description = description
    },
    SET_ROYALTIES(state, royalties){
      state.royalties = royalties
    },
    SET_TICKET(state, ticket){
      state.ticket = ticket
    },
    SET_NSFW(state, nsfw){
      state.nsfw = nsfw
    },
    SET_CODEC(state, codec){
      state.codec = codec
    },
    SET_PROGRESS(state, progress){
      state.progress = progress
    },
    SET_UPLOADING(state, uploading){
      state.uploading = uploading
    },
    SET_FEED(state, feed){
      state.feed = feed
    },
    SET_NFT(state, nft){
      state.nft = nft
    },
    SET_INITX(state, qurl){
      state.initTxQurl = qurl
    },
    SET_INITX_WS(state, initx){
      state.initTxWs = initx
    },
    SET_WSD(state, wsd){
      state.wsd = wsd
    },
    SET_ACC(state, acc){
      state.acc = acc
    },
    SET_UTOKEN(state, utoken){
      state.utoken = utoken
    },
    SET_REMEMBER(state, remember){
      state.remember = remember
    }
  }
})