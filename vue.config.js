// const IS_PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
    outputDir: 'dist',
    assetsDir: 'static',
    configureWebpack: {
      resolve: {
          fallback: {
            "fs": false,
            "stream": false 
          }
      }
    },

    // baseUrl: IS_PRODUCTION
    // ? 'http://cdn123.com'
    // : '/',
    // For Production, replace set baseUrl to CDN
    // And set the CDN origin to `yourdomain.com/static`
    // Whitenoise will serve once to CDN which will then cache
    // and distribute
    devServer: {
      proxy: {
        '/api*': {
          // Forward frontend dev server request for /api to django dev server
          target: 'http://localhost:8000/',
        }
      }
    }
  }

// module.exports = {
//   chainWebpack: config => {
//     config.resolve.alias.set('vue', '@vue/compat')

//     config.module
//       .rule('vue')
//       .use('vue-loader')
//       .tap(options => {
//         return {
//           ...options,
//           compilerOptions: {
//             compatConfig: {
//               MODE: 2
//             }
//           }
//         }
//       })
//     }
// }